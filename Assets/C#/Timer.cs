using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    public string sceneToLoad;
    public TextMeshProUGUI timerText;
    private float startTime;
    private ScoreManager scoreManager;
    private int initialMinutes = 19; 

    void Start()
    {
        startTime = Time.time;
        ResetMinutes(); 
    }

    public void ResetMinutes()
    {
        int minutes = initialMinutes;
        int seconds = 0;
        timerText.text = "Time: " + minutes.ToString("D2") + ":" + seconds.ToString("D2");
    }

    void Update()
    {
        float elapsedTime = Time.time * 2 - startTime;
        int minutes = initialMinutes + (int)(elapsedTime / 60f);
        int seconds = (int)(elapsedTime % 60f);
        timerText.text = "Time: " + minutes.ToString("D2") + ":" + seconds.ToString("D2");

        if (minutes == 22 && seconds == 0)
        {
            SceneManager.LoadScene(sceneToLoad);
            ResetMinutes(); 
        }
    }
}