using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FadeAwayScene : MonoBehaviour
{
    public float fadetime;

    private Image fadeImage;

    public float Value;

    private float fadePersec;
    
    void Start()
    {
        fadeImage = GetComponent<Image>();
        fadePersec = 1 / fadetime;
        Value = fadeImage.color.a;
    }

    
    void Update()
    {
        if (fadetime > 0)
        {
            Value -= fadePersec * Time.deltaTime;
            fadeImage.color = new Color(fadeImage.color.r, fadeImage.color.g, fadeImage.color.b, Value);
            fadetime -= Time.deltaTime;
        }
        
        
    }
}