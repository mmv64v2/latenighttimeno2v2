using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GoHomeSafe : MonoBehaviour
{
    private Timer _timer;
    public string sceneToLoad;
    public GameObject TIme;

    private void Start()
    {
        _timer = TIme.GetComponent<Timer>();
    }

    private void OnTriggerEnter(Collider other)
    {
        
        
        if (other.CompareTag("Player"))
        {
            _timer.ResetMinutes();
            SceneManager.LoadScene(sceneToLoad);
            
        }
    }
}
