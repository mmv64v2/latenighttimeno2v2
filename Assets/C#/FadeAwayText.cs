using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FadeAwayText : MonoBehaviour
{
    public float fadetime;

    private TextMeshProUGUI fadeText;

    public float Value;

    private float fadePersec;
    
    void Start()
    {
        fadeText = GetComponent<TextMeshProUGUI>();
        fadePersec = 1 / fadetime;
        Value = fadeText.color.a;
    }
    void Update()
    {
        if (fadetime > 0)
        {
            Value -= fadePersec * Time.deltaTime;
            fadeText.color = new Color(fadeText.color.r, fadeText.color.g, fadeText.color.b, Value);
            fadetime -= Time.deltaTime;
        }
    }
}
