using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    public float detectionRange = 10f; 
    private Transform target;         
    private NavMeshAgent navMeshAgent;
    public Transform playerTransform;
    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        

        
        target = playerTransform;
    }

    void Update()
    {
       
        if (target != null)
        {
            
            float distanceToTarget = Vector3.Distance(transform.position, target.position);

            Debug.Log("Distance to target: " + distanceToTarget);

           
            if (distanceToTarget <= detectionRange)
            {
                Debug.Log("Chasing player!");
              
                navMeshAgent.SetDestination(target.position);
            }
        }

        if (target == null)
        {
            Debug.Log("targetnull");
        }
    }
    public void SetTarget(Transform newTarget)
    {
        target = newTarget;
    }
}