using UnityEngine;

public class Ghostpingpong : MonoBehaviour
{
    public float moveSpeed = 5.0f; 
    public float moveDistance = 5.0f; 

    private Vector3 startPosition;
    private bool movingRight = true;

    void Start()
    {
        startPosition = transform.position;
    }

    void Update()
    {
        if (movingRight)
        {
            transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
        }

       
        if (Vector3.Distance(startPosition, transform.position) >= moveDistance)
        {
           
            movingRight = !movingRight;
        }
    }
}