using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileSpawner : MonoBehaviour
{
    public AudioSource reloadSound;
    public AudioSource Hitsound;
    public GameObject projectilePrefab;
    public Transform launchPoint;
    public float shotDelay = 1.0f;
    public int maxAmmo = 5;
    public int currentAmmo;
    public float projectilePower = 10.0f;
    public KeyCode reloadKey = KeyCode.R;
    public AmmoUIUpdater ammoUIUpdater;
    private float lastShotTime;
    public float power = 10.0f;
    public float reloadTime = 1.2f;
    private bool isReloading = false;

    void Start()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        if (rb != null)
        {
            rb.AddForce(transform.forward * power, ForceMode.Impulse);
        }
        currentAmmo = maxAmmo;
    }

    private void Update()
    {
        if (!isReloading)
        {
            if (Input.GetMouseButtonDown(0) && currentAmmo > 0 && Time.time - lastShotTime >= shotDelay)
            {
                
                Shoot();
                Hitsound.Play();
                lastShotTime = Time.time;
            }

            if (Input.GetKeyDown(reloadKey) && currentAmmo < maxAmmo)
            {
                reloadSound.Play();
                StartReloading();
            }
        }
    }

    void Shoot()
    {
        GameObject newProjectile = Instantiate(projectilePrefab, launchPoint.position, launchPoint.rotation);
        Rigidbody rb = newProjectile.GetComponent<Rigidbody>();
        if (rb != null)
        {
            rb.AddForce(transform.forward * power, ForceMode.Impulse);
            
        }
        currentAmmo--;

        if (currentAmmo <= 0)
        {
            currentAmmo = 0;
        }
    }

    void StartReloading()
    {
        StartCoroutine(Reload());
    }


    IEnumerator Reload()
    {
        isReloading = true;
        yield return new WaitForSeconds(reloadTime);
        
        int bulletsToAdd = maxAmmo - currentAmmo;
        currentAmmo += bulletsToAdd;

        isReloading = false;
        ammoUIUpdater.UpdateAmmoUI();
    }
    
    
}
