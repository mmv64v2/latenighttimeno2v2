using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public TextMeshProUGUI scoreText;
    public int score = 0;
    public string UnloadScene;
    private Reward _reward;

    private bool sceneChangePending = false;

    void Start()
    {
        _reward = FindObjectOfType<Reward>();
        UpdateScoreText();
    }

    public void AddScore(int points)
    {
        score += points;
        UpdateScoreText();
    }

    private void UpdateScoreText()
    {
        scoreText.text = "Score: " + score;
    }

    private void Update()
    {
        UpdateScoreText();
        if (score >= 100)
        {
            if (_reward != null)
            {
                if (!sceneChangePending)
                {
                    
                    StartCoroutine(ChangeSceneWithDelay());
                }
            }
        }

        if (_reward == null)
        {
            Debug.Log("reward not found");
        }
    }

    private IEnumerator ChangeSceneWithDelay()
    {
        sceneChangePending = true;
        yield return new WaitForSeconds(1f); 
        _reward.AddReward(1);
        SceneManager.LoadScene(UnloadScene);
        score = 0;
    }
}