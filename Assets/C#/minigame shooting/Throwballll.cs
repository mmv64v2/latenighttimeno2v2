using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Throwballll : MonoBehaviour
{
    public GameObject ballPrefab; 
    private bool canThrow = true;

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && canThrow)
        {
            ThrowBall();
            canThrow = false;
            float randomForce = Random.Range(10f, 15f); 
            Invoke("EnableThrowing", 2f);
        }
    }
    
    void ThrowBall()
    {
        GameObject ball = Instantiate(ballPrefab, transform.position + Vector3.left, Quaternion.identity);

        Rigidbody rb = ball.GetComponent<Rigidbody>();
        if (rb != null)
        {
            float randomForce = Random.Range(10f, 15f); 
            rb.AddForce(transform.forward * randomForce, ForceMode.Impulse);
        }
    }
    
    void EnableThrowing()
    {
        canThrow = true;
    }
}