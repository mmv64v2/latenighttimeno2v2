using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDestroyer : MonoBehaviour

{
    
    public float destroyDelay = 1.0f; 

    void Start()
    {
        
        StartCoroutine(DestroyObjectDelayed());
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject)
        {
           
        }
    }
    IEnumerator DestroyObjectDelayed()
    {
        
        yield return new WaitForSeconds(destroyDelay);

       
        Destroy(gameObject);
    }
}

