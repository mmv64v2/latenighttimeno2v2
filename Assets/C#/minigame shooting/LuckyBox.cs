using UnityEngine;
using UnityEngine.SceneManagement;

public class LuckyBox : MonoBehaviour
{
    private Reward _reward;
    public GameObject[] items;
    public string winSceneName;
   
    private Camera mainCamera;

    private void Start()
    {
        mainCamera = Camera.main;
        _reward = FindObjectOfType<Reward>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            SpawnRandomItemInFrontOfView();
        }
    }

    void SpawnRandomItemInFrontOfView()
    {
        if (items.Length > 0)
        {
            int randomIndex = Random.Range(0, items.Length);

            Vector3 spawnOffset = mainCamera.transform.forward * 2.0f;
            Vector3 spawnPosition = mainCamera.transform.position + spawnOffset;
            Quaternion spawnRotation = Quaternion.Euler(45, 0, 0);

            GameObject spawnedItem = Instantiate(items[randomIndex], spawnPosition, spawnRotation);

            if (spawnedItem.CompareTag("WIN"))
            {
                _reward.AddReward(1);
                Invoke("ChangeSceneAfterDelay", 1f);
            }
            if (spawnedItem.CompareTag("Lose"))
            {
                Invoke("ChangeSceneAfterDelay", 1f);
            }
        }
    }

    void ChangeSceneAfterDelay()
    {
        SceneManager.LoadScene(winSceneName);
    }
}