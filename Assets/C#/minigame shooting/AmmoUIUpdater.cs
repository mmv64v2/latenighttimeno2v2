using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AmmoUIUpdater : MonoBehaviour
{
    public TextMeshProUGUI ammoText; 
    public ProjectileSpawner objectShooter; 

    private void Start()
    {
        
        UpdateAmmoUI();
    }

    private void Update()
    {
        
        UpdateAmmoUI();
    }

    public void UpdateAmmoUI()
    {
        
        if (ammoText != null && objectShooter != null)
        {
           
            ammoText.text = "Ammo: " + objectShooter.currentAmmo + "/" + objectShooter.maxAmmo;
        }
    }
}
