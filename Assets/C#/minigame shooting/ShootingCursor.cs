using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingCursor : MonoBehaviour
{
  
       public Texture2D defaultCursor; 
       public Texture2D shootingCursor; 
       private bool isShooting;
   
       void Start()
       {
           
           Cursor.SetCursor(defaultCursor, Vector2.zero, CursorMode.Auto);
       }
   
       void Update()
       {
           
           if (Input.GetMouseButtonDown(0)) 
           {
              
               SetCursor(shootingCursor);
               isShooting = true;
           }
           else if (isShooting)
           {
               
               SetCursor(defaultCursor);
               isShooting = false;
           }
       }
   
       private void SetCursor(Texture2D cursorTexture)
       {
           Cursor.SetCursor(cursorTexture, Vector2.zero, CursorMode.Auto);
       }
   }


