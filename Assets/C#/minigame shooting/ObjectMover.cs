using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMover : MonoBehaviour
{
    public float speed = 2.0f;
    public float distance = 5.0f;
    private Vector3 startPosition;
    private float direction = 1;
   

    void Start()
    {
        startPosition = transform.position;
    }

    void Update()
    {
       
            float newPosition = Mathf.PingPong(Time.time * speed, distance) * direction;
            transform.position = startPosition + Vector3.right * newPosition;

            if (newPosition >= distance || newPosition <= 0)
            {
                direction *= -1;
            }
       
    }
}