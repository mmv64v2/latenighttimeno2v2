using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playsound : MonoBehaviour
{
    public AudioSource BGSound;

    public bool isLoop = true;
    // Start is called before the first frame update
    void Start()
    {
        BGSound.Play();
        isLoop = BGSound.loop;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
