using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Checkreward : MonoBehaviour
{
    public TextMeshProUGUI toShow;
    public TextMeshProUGUI tohide;
    private Reward Re;
    public GameObject player;

    private void Start()
    {
        
        Re = player.GetComponent<Reward>();
    }
    
    private void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Player"))
        {
            toShow.gameObject.SetActive(true);
            tohide.gameObject.SetActive(false);
            Re.Checkreward();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            toShow.gameObject.SetActive(false);
            tohide.gameObject.SetActive(true);
            Re.Checkreward();
        }
    }
}
