using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine;

public class discoll : MonoBehaviour
{
    private ScoreManager scoreManager;

    private void Start()
    {
        scoreManager = FindObjectOfType<ScoreManager>(); 
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Hoop"))
        {
            
            if (scoreManager != null)
            {
                scoreManager.AddScore(20); 
            }
            
            Destroy(gameObject);
        }
    }
}

