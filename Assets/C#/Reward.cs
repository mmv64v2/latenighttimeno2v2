using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class Reward : MonoBehaviour
{
    public TextMeshProUGUI rewardtext;
    public int reward = 0;
    public Transform objectToMove; 

    void Start()
    {
        LoadReward();
        UpdateRewardText();
    }

    public void AddReward(int points)
    {
        reward += points;
        SaveReward();
        UpdateRewardText();

        
    }

    private void UpdateRewardText()
    {
        rewardtext.text = ": " + reward;
    }

    private void SaveReward()
    {
        PlayerPrefs.SetInt("Reward", reward);
    }

    private void LoadReward()
    {
        if (PlayerPrefs.HasKey("Reward"))
        {
            reward = PlayerPrefs.GetInt("Reward");
        }
    }

    public void Checkreward()
    {
        if (reward >= 2)
        {
            Vector3 newPosition = objectToMove.position + new Vector3(16f, 0f, 0f);
            objectToMove.position = newPosition;
        }

        reward = 0;
    }
    void Update()
    {
        
    }

    private void OnApplicationQuit()
    {
        ResetReward();
    }

    public void ResetReward()
    {
        reward = 0;
        SaveReward();
        UpdateRewardText();
    }

    private void OnDisable()
    {
        SaveReward();
    }
}