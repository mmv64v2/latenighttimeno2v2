using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using TMPro;
using G3;

public class Sceneshoting : MonoBehaviour
{
    [SerializeField] protected TextMeshProUGUI m_InteractionTxt;
    public string sceneToLoad; 
    private ScoreManager scoreManager;
    private bool canChangeScene = false;

    private void Update()
    {
        if (canChangeScene && Input.GetKeyDown(KeyCode.E))
        {
            SceneManager.LoadScene(sceneToLoad);
        }
        
        
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Player"))
        {
            m_InteractionTxt.gameObject.SetActive(true);
            canChangeScene = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        
        if (other.CompareTag("Player"))
        {
            m_InteractionTxt.gameObject.SetActive(false);
            canChangeScene = false;
        }
    }
}

